#!/bin/bash
set -e

bundle exec rails db:seed

exec "$@"
