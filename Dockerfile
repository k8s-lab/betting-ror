FROM ruby:2.5.0-alpine3.7

ENV HELM_LATEST_VERSION="v2.6.2"
ENV DRAFT_LATEST_VERSION="v0.7.0"
ENV DATABASE mongo
ENV PATH /root/.yarn/bin:$PATH

# Minimal requirements to run a Rails app
RUN apk add --no-cache --update build-base \
                                linux-headers \
                                bash \
                                nodejs \
                                openssh \
                                git \
                                postgresql-dev \
                                tzdata \
  && apk add curl bash binutils tar gnupg \
  && rm -rf /var/cache/apk/* \
  && /bin/bash \
  && touch ~/.bashrc \
  && curl -o- -L https://yarnpkg.com/install.sh | bash \
  && apk del tar binutils

ENV APP_PATH /usr/src/app

WORKDIR $APP_PATH
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
#ENV RAILS_ENV=production
#RUN bundle install --deployment --without development test --jobs "$(getconf _NPROCESSORS_ONLN)" --retry 3
RUN bundle install --deployment --without test --jobs "$(getconf _NPROCESSORS_ONLN)" --retry 3
COPY . $APP_PATH
RUN yarn install
#RUN npm rebuild node-sass --force
#RUN bundle exec rails assets:precompile
CMD ["bash", "-c", "rails db:seed && rails s"]

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.9.4/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl

RUN curl -LO  https://storage.googleapis.com/kubernetes-helm/helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz && \
    tar -xvf helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz && mv linux-amd64/helm /usr/local/bin && \
    rm helm-${HELM_LATEST_VERSION}-linux-amd64.tar.gz

RUN curl -LO  https://github.com/Azure/draft/releases/download/${DRAFT_LATEST_VERSION}/draft-${DRAFT_LATEST_VERSION}-linux-amd64.tar.gz && \
    tar -xvf draft-${DRAFT_LATEST_VERSION}-linux-amd64.tar.gz && mv linux-amd64/draft /usr/local/bin && \
    rm draft-${DRAFT_LATEST_VERSION}-linux-amd64.tar.gz

RUN apk add --no-cache python3 openssh-client iptables tini conntrack-tools git curl torsocks sshfs vim && \
    git clone https://github.com/datawire/telepresence.git && \
    cd telepresence && env PREFIX=/usr/local ./install.sh

COPY kube.conf /root/.kube/config
