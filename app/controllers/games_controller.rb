class GamesController < ApplicationController
  before_action :set_game, only: [:show, :bet]
  before_action :set_username

  def index
    date_from_param = date_filter_params[:travel_date].to_date unless date_filter_params[:travel_date].nil?
    @travel_date = date_from_param || session[:travel_date]&.to_date || Date.today
    session[:travel_date] = @travel_date

    @past_games = PastGameDecorator.wrap_collection(Game.finished(@travel_date), @username)
    @upcoming_games = UpcomingGameDecorator.wrap_collection(Game.upcoming(@travel_date), @username)

    @bet_won = Game.finished(@travel_date).map { |g| g.winning_bet(@username) }.flatten.count
    @travel_direction = date_filter_params[:travel_direction] || 'forward'
  end

  def time_travel
    redirect_to(games_url(date_filter: date_filter_params))
  end

  def show
  end

  def username
    session[:username] = user_params[:name]
    redirect_to(games_url(date_filter: date_filter_params))
  end

  def bet
    if session[:username].nil?
      redirect_to games_path, notice: 'Please set username!'
    elsif @game.bets.user_bet(@username).any?
      redirect_to game_path(@game.id), notice: 'You already bet'
    else
      @game.bets << Bet.new(bet_on: params[:team], user: @username)
      redirect_to game_path(@game.id)
    end
  end

  private

  def set_game
    game = Game.find(params[:id])
    if game.date < Date.parse(session[:travel_date])
      @game = PastGameDecorator.wrap(game, @username)
    else
      @game = UpcomingGameDecorator.wrap(game, @username)
    end
  end

  def set_username
    @username = session[:username]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def game_params
    params.require(:game).permit(:equipea, :equipeb, :date, :scorea, :scoreb)
  end

  def user_params
    params.require(:user).permit(:name)
  end

  def date_filter_params
    params.fetch(:date_filter, {}).permit(:travel_date, :travel_direction)
  end
end
