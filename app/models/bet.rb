class Bet
  include Mongoid::Document
  field :bet_on, type: String
  field :user, type: String
  scope :user_bet, ->(user) { where(user: user) }
  scope :equipe_bet, ->(equipe) { where(bet_on: equipe) }
  embedded_in :game
end
