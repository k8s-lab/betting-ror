class PastGameDecorator
  attr_reader :game

  delegate :id, :date, :equipea, :equipeb, :score, :to_model, :bets, :bets_on_equipea, :bets_on_equipeb, :equipea_color, :equipeb_color, :winning_bet, to: :game

  def self.wrap_collection(collection, username)
    collection.map{ |obj| new obj, username }
  end

  def self.wrap(obj, username)
    new obj, username
  end

  def initialize(game, username)
    @game = game
    @username = username
  end

  def show_title
    user_has_bets? ? 'Gains' : 'Details'
  end

  def bet_allowed?
    false
  end

  def row_style
    return 'table-success' if user_has_a_winning_bet?
    return 'table-danger' if user_has_bets? && user_has_a_losing_bet?
  end

  private

  def user_has_a_winning_bet?
    winning_bet(@username).any?
  end

  def user_has_a_losing_bet?
    winning_bet(@username).empty?
  end

  def user_has_bets?
    bets.user_bet(@username).any?
  end
end
