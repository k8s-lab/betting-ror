json.extract! game, :id, :equipea, :equipeb, :date, :scorea, :scoreb, :created_at, :updated_at
json.url game_url(game, format: :json)
