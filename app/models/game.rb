class Game
  include Mongoid::Document
  embeds_many :bets
  default_scope { order_by(date: 'asc') }
  scope :upcoming, ->(at_date) { where(:date.gte => at_date) }
  scope :finished, ->(at_date) { where(:date.lt => at_date) }
  field :equipea, type: String
  field :equipeb, type: String
  field :date, type: Date
  field :scorea, type: String
  field :scoreb, type: String


  def bets_on_equipea
    bets.where(bet_on: equipea).count
  end

  def bets_on_equipeb
    bets.where(bet_on: equipeb).count
  end

  def score
    "#{scorea} - #{scoreb}"
  end

  def equipea_color
    return 'bg-success' if equipea_win?
  end

  def equipeb_color
    return 'bg-success' if equipeb_win?
  end

  def equipea_win?
    scorea > scoreb
  end

  def equipeb_win?
    scoreb > scorea
  end

  def winner
    return equipea if scorea > scoreb
    return equipeb if scoreb > scorea
    nil
  end

  def winning_bet(user)
    bets.user_bet(user).equipe_bet(winner)
  end
end
