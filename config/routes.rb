Rails.application.routes.draw do
  resources :games, only: [:index, :show] do
    member do
      post 'bet/(:team)', to: 'games#bet', as: 'bet'
    end
  end
  post 'games', to: 'games#time_travel'
  post 'username', to: 'games#username'

  root to: 'games#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
