# frozen_string_literal: true
if Game.empty?
  TEAMS = %w[Nantes Paris-SG Monaco Marseille Lyon Montpellier Nice
           Bordeaux Dijon Guingamp Caen Saint-éteinne Angers Strasbourg Amiens
           Toulouse Troyes Lille Metz]

  MAX_SCORE = 5

  10.times do |i|
    Game.create(equipea: 'Rennes', equipeb: TEAMS.sample,
                date: Date.today - rand(i),
                scorea: rand(MAX_SCORE), scoreb: rand(MAX_SCORE))

    Game.create(equipea: 'Rennes', equipeb: TEAMS.sample,
                date: Date.today + rand(i),
                scorea: rand(MAX_SCORE), scoreb: rand(MAX_SCORE))
  end
end
