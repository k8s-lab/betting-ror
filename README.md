# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version **2.5.0**

* Configuration

use DATABASE en variable to point to your mongo database

* Database creation

This is a mongodb so it will be created if needed

* Database initialization

rails db:seed

* How to run the test suite

rails test

* Run locally
docker-compose up


* Dev

overmind or foreman start

then rails s

This will start mongodb and rails server

Run telepresence from your computer

DATABASE=mongo telepresence --swap-deployment betting --expose 3000 --method=inject-tcp --run rails s

from inside container: jeanfrancoislabbe/breizhcamp_betting:latest

```bash
docker run -v /home/jeff/.minikube/:/home/jeff/.minikube -v ~/.kube:/root/.kube --rm -ti jeanfrancoislabbe/breizhcamp_betting:latest bash

bash-4.4# DATABASE=mongo telepresence --swap-deployment betting --expose 3000 --method=inject-tcp --run rails s
```
