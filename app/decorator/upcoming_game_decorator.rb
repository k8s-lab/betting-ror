class UpcomingGameDecorator
  attr_reader :game

  delegate :id, :date, :equipea, :equipeb, :to_model, :bets, :bets_on_equipea, :bets_on_equipeb, to: :game

  def self.wrap_collection(collection, username)
    collection.map { |obj| new obj, username }
  end

  def self.wrap(obj, username)
    new obj, username
  end

  def initialize(game, username)
    @game = game
    @username = username
  end

  def score
    '-'
  end

  def show_title
    bets.user_bet(@username).any? ? 'Parié' : 'Pariez'
  end

  def bet_allowed?
    true
  end

  def equipea_color; end
  def equipeb_color; end

  def row_style
    return 'table-info' if user_has_bets?
  end

  def user_has_bets?
    bets.user_bet(@username).any?
  end
end
